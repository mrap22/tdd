from django.db import models
from django import forms
import datetime
from django.utils import timezone

class Status(models.Model):
    isi = models.CharField(max_length=300)
    waktu = models.DateTimeField(auto_now_add=True, blank=True)

    def __str__(self):
        return self.nama
