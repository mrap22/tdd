from django.shortcuts import render
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.views.generic import TemplateView

from .models import Status
from .forms import FormStatus

# Create your views here.
def index(request):
    text = ''
    if request.method == "POST":
        form = FormStatus(request.POST)
        if form.is_valid():
            form.save()
            for i in Status.objects.all():
                text = text + '(' + i.waktu.strftime("%d-%m-%Y %H:%M") + ')\n' + i.isi +'\n\n'
                return HttpResponseRedirect(request.path, {'txt' : text})

    form = FormStatus()
    for i in Status.objects.all():
        text = text + '(' + i.waktu.strftime("%d-%m-%Y %H:%M") + ')\n' + i.isi +'\n\n'

    context = {'form': form, 'txt': text,}

    return render(request, "main/landingpage.html", context)
