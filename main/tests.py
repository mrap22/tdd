from django.test import TestCase
from django.test import Client
from django.urls import resolve
#from .views import index, mhs_name, calculate_age
from django.http import HttpRequest
#from datetime import date
import unittest
import datetime
from .models import Status

class labppw2UnitTest(TestCase):

    def setUp(self):
        self.client = Client()
        self.now = datetime.datetime.now()
        Status.objects.create(isi='testing', waktu=self.now)

    def test_using_index_func(self):
        found = resolve('/main/')
        self.assertEqual(found.view_name, 'main')

    def test_landing_page_is_completed(self):
        response = self.client.get('/main/')
        self.assertEqual(response.status_code,200)

    def test_model_database_successful_input(self):
        database = Status.objects.all()
        self.assertEqual(len(database), 1)

    def test_model_database_successful_retrieval(self):
        getItem = Status.objects.all()[0]
        self.assertEqual(getItem.isi, 'testing')
        self.assertEqual(getItem.waktu, self.now)

    def test_html_template_loaded(self):
        response = self.client.get('/main/')
        self.assertTemplateUsed(response, 'landingpage.html')
