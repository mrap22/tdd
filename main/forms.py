from django import forms
from django.forms import ModelForm

from .models import Status

class FormStatus(ModelForm):
    isi = forms.CharField(widget=forms.Textarea, label='')
    class Meta:
        model = Status
        fields = ['isi']
