from django.conf.urls import url
from .views import index
from . import views

urlpatterns = [
    url('', views.index, name='main'),
]
