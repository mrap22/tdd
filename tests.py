import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

class Lab7TestCase(unittest.TestCase):

    def setUp(self):
        self.browser = webdriver.Chrome()
        driver = self.browser
        self.browser.get("https://ppw-lab5-ttd.herokuapp.com/main")

    def test_page_title(self):
        self.assertEqual("Story 6", self.browser.title)

    def test_check_heloapakabar_exists(self):
        welcometext = self.browser.find_element_by_tag_name('h1')
        self.assertEqual(welcometext.text, "Hello apa kabar?")

    def test_fill_form_and_check_database_update(self):
        textarea = self.browser.find_element_by_id('id_isi')
        submit = self.browser.find_element_by_xpath("//input[@type='submit']")
        textarea.send_keys("Coba Coba")
        submit.click()
        result = self.browser.find_element_by_class_name("displaytext")
        self.assertIn("Coba Coba", result.text)

    def test_css_header_text_color(self):
        welcometext = self.browser.find_element_by_tag_name('h1')
        self.assertEqual(welcometext.value_of_css_property("color"), "rgba(255, 165, 0, 1)")

    def test_css_check_footer_text_color(self):
        column = self.browser.find_element_by_tag_name('h4')
        self.assertEqual(column.value_of_css_property("color"), "rgba(192, 192, 192, 1)")




    def tearDown(self):
        self.browser.quit()



if __name__ == "__main__":
    unittest.main()
