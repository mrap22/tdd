from django.shortcuts import render
from django.http import HttpResponse
from django.http import HttpResponseRedirect
# Create your views here.

def index(request):
    context = {'tablegoeshere': 'initabel', 'numberofbooksgoeshere': "thisisthenumberofbooks",}
    return render(request, "book/book.html", context)
